
var through = require('through2');
var compiler  = require('vue-component-compiler')
var gutil = require('gulp-util');
var path = require('path');
var merge = require('merge');

var PluginError = gutil.PluginError;

module.exports = function (opt) {
  function replaceExtension(path) {
    return gutil.replaceExtension(path, '.js');
  }

  function transform(file, enc, cb) {
    if (file.isNull()) return cb(null, file);
    if (file.isStream()) return cb(new PluginError('gulp-vue', 'Streaming not supported'));

    var data;
    var str = file.contents.toString('utf8');
    var dest = replaceExtension(file.path);


    compiler.compile(str, function (err, result) {

      if (err) return cb(new PluginError('gulp-vue', err));

      file.contents = new Buffer(result);
      file.path = dest;
      cb(null, file);
    });
  }

  return through.obj(transform);
};
